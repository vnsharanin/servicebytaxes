﻿drop database ws_penalties_data;
create database ws_penalties_data;
use ws_penalties_data;

create table Penalties (
   id BIGINT NOT NULL AUTO_INCREMENT,

   number NVARCHAR(10) NOT NULL,
   date NVARCHAR(256) NOT NULL,
   branch NVARCHAR(256) NOT NULL,
   document NVARCHAR(256) NOT NULL,
   sum INT NULL,
   is_payed TINYINT DEFAULT 0,

   PRIMARY KEY (id)
);

INSERT INTO Penalties(number,date, branch, document,sum,is_payed)
VALUES
	('T188HM33','11.06.17','VLADIMIR','SN505050',0,0)
,	('T188HM33','11.06.17','VLADIMIR','SN505051',100,1)
,	('X849MO33','11.06.17','VLADIMIR','SN505052',100,0)
,	('X849MO33','11.06.17','VLADIMIR','SN505053',0,1);