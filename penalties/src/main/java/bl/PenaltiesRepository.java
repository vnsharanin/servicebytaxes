package bl;

import javax.annotation.PostConstruct;
import java.util.*;
import com.google.common.collect.Lists;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import bl.dao.*;
import bl.domain.*;

import io.spring.getter_penalties_web_service.Penalty;
import io.spring.getter_penalties_web_service.GetPenaltiesResponse;

@Component
public class PenaltiesRepository {

	@Autowired // This means to get the bean called penaltyRepositoryDao
	           // Which is auto-generated by Spring, we will use it to handle the data
	private PenaltyRepositoryDao penaltyRepositoryDao;

	@PostConstruct
	public void initData() {
	}

	public GetPenaltiesResponse findPenalties(List<String> numbers) {
		Assert.notNull(numbers.get(0), "The numbers must not be null");

		for (String number : numbers)
			System.out.println(number);
			
		//тянем все нарушения
		List<PenaltyDao> penaltiesdb = Lists.newArrayList(penaltyRepositoryDao.findAll());
		//объявляем объект ответа
		GetPenaltiesResponse response = new GetPenaltiesResponse();
		//подготавливаем список нарушений
		for (PenaltyDao penaltydb : penaltiesdb) {
			System.out.println(penaltydb.getNumber());
			for (String number : numbers){
				if (penaltydb.getNumber().equals(number)) {
					Penalty penalty = new Penalty();
					penalty.setDate(penaltydb.getDate());
					penalty.setNumber(penaltydb.getNumber());
					penalty.setBranch(penaltydb.getBranch());
					penalty.setDocument(penaltydb.getDocument());
					penalty.setSum(penaltydb.getSum());
					penalty.setIsPayed(penaltydb.getIsPayed());
					response.getPenalties().add(penalty);
					break;
				}
			}
		}

		//тест на добавление в базу, а надо получить список
		/*PenaltyDao newPenalty = new PenaltyDao();
		newPenalty.setDate(String.valueOf(new java.util.Date()));
		newPenalty.setNumber("TEST");
		newPenalty.setBranch("TEST");
		newPenalty.setDocument("TEST");
		newPenalty.setSum(50);
		newPenalty.setIsPayed(false);
		penaltyRepositoryDao.save(newPenalty);*/

		//Возвращаем ответ, который содержит список нарушений
		return response;
	}
}
