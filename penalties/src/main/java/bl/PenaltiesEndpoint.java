package bl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import io.spring.getter_penalties_web_service.GetPenaltiesRequest;
import io.spring.getter_penalties_web_service.GetPenaltiesResponse;

@Endpoint
public class PenaltiesEndpoint {
	private static final String NAMESPACE_URI = "http://spring.io/getter-penalties-web-service";

	private PenaltiesRepository penaltiesRepository;

	@Autowired
	public PenaltiesEndpoint(PenaltiesRepository penaltiesRepository) {
		this.penaltiesRepository = penaltiesRepository;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPenaltiesRequest")
	@ResponsePayload
	public GetPenaltiesResponse getPenalties(@RequestPayload GetPenaltiesRequest request) {
		return penaltiesRepository.findPenalties(request.getNumber());
	}
}
