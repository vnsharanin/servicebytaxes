package bl.dao;

import org.springframework.data.repository.CrudRepository;

import bl.domain.PenaltyDao;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface PenaltyRepositoryDao extends CrudRepository<PenaltyDao, Integer> {

}