package bl.dao;

import org.springframework.data.repository.CrudRepository;

import bl.domain.PassportDao;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface PassportRepositoryDao extends CrudRepository<PassportDao, Integer> {

}