package bl.domain;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
//import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "Passports")
public class PassportDao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "seria", nullable = false)
    private Integer seria;

    @Column(name = "number", nullable = false)
    private Integer number;

    @Column(name = "department", nullable = true)
    private String department;

    @Column(name = "date_of_issue", nullable = true)
    private String dateOfIssue;

    @Column(name = "code", nullable = true)
    private String code;

    @Column(name = "secondname", nullable = true)
    private String secondname;

    @Column(name = "firstname", nullable = true)
    private String firstname;

    @Column(name = "thirdname", nullable = true)
    private String thirdname;

    @Column(name = "gender", nullable = true)
    private String gender;

    @Column(name = "birthdate", nullable = true)
    private String birthdate;

    @Column(name = "birthplace", nullable = true)
    private String birthplace;

    @Column(name = "identify_string", nullable = true)
    private String identify_string;

    @Column(name = "inn", nullable = true)
    private String INN;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSeria() {
        return seria;
    }

    public void setSeria(Integer seria) {
        this.seria = seria;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(String dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSecondname() {
        return secondname;
    }

    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getThirdname() {
        return thirdname;
    }

    public void setThirdname(String thirdname) {
        this.thirdname = thirdname;
    }

    public String getGender() {
        return gender;
    }

    public void setGendre(String gender) {
        this.gender = gender;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    public String getIdentify_string() {
        return identify_string;
    }

    public void setIdentify_string(String identify_string) {
        this.identify_string = identify_string;
    }

    public String getINN() {
        return INN;
    }

    public void setINN(String INN) {
        this.INN = INN;
    }

}
