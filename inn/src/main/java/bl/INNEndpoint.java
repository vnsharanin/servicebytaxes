package bl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import io.spring.getter_inn_web_service.GetINNRequest;
import io.spring.getter_inn_web_service.GetINNResponse;

@Endpoint
public class INNEndpoint {
	private static final String NAMESPACE_URI = "http://spring.io/getter-inn-web-service";

	private INNRepository innRepository;

	@Autowired
	public INNEndpoint(INNRepository innRepository) {
		this.innRepository = innRepository;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getINNRequest")
	@ResponsePayload
	public GetINNResponse getINN(@RequestPayload GetINNRequest request) {
		GetINNResponse response = new GetINNResponse();
		response.setINN(innRepository.findINN(request.getPassport()));

		return response;
	}
}
