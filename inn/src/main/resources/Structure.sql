﻿drop database ws_inn_data;
create database ws_inn_data;
use ws_inn_data;

create table Passports (
   id BIGINT NOT NULL AUTO_INCREMENT,

   seria INT NOT NULL,
   number INT NOT NULL,
   department NVARCHAR(256) NULL,
   date_of_issue NVARCHAR(256) NULL,
   code NVARCHAR(256) NULL,
   secondname NVARCHAR(256) NULL,
   firstname NVARCHAR(256) NULL,
   thirdname NVARCHAR(256) NULL,
   gender NVARCHAR(6) NULL,
   birthdate NVARCHAR(256) NULL,
   birthplace NVARCHAR(256) NULL,
   identify_string NVARCHAR(256) NULL,
   inn NVARCHAR(256) NULL,
   PRIMARY KEY (id),
   UNIQUE KEY(seria,number)
);

INSERT INTO Passports(seria, number, department,date_of_issue,code,secondname,firstname,thirdname,gender,birthdate,identify_string,inn)
VALUES
	(1714,950951,'UFMS ROSSII PO VLAD.OMLASTI','12.05.15','330-015','Sharanin','Vladimir','Nikolaevich','MALE','21.03.95','','1234567890')
,	(1714,950952,'UFMS ROSSII PO VLAD.OMLASTI','13.05.15','330-015','Novaya','Zemlya','Vladimirovna','FEMALE','22.03.95','','1234567891')
,	(1714,950953,'UFMS ROSSII PO VLAD.OMLASTI','14.05.15','330-015','Sharanin2','Vladimir2','Nikolaevich2','MALE','23.03.95','','1234567892')
,	(1714,950954,'UFMS ROSSII PO VLAD.OMLASTI','15.05.15','330-015','Sharanin3','Vladimir3','Nikolaevich3','MALE','24.03.95','','1234567893');