mvn clean package
java -jar target/getter-inn-web-service-0.1.0.jar --server.port=8080
java -jar target/getter-taxes-web-service-0.1.0.jar --server.port=8090
java -jar target/getter-penalties-web-service-0.1.0.jar --server.port=8095

http://localhost:8080/wsINN
http://localhost:8090/wsTaxes
http://localhost:8095/wsPenalties

http://localhost:8080/wsINN/INN.wsdl
http://localhost:8090/wsTaxes/taxes.wsdl
http://localhost:8095/wsPenalties/penalties.wsdl


soapclient
mvn generate-sources
mvn compile
mvn -Dexec.mainClass=net.webservicex.MainClassForCalls exec:java