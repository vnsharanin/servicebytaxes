package bl.dao;

import org.springframework.data.repository.CrudRepository;

import bl.domain.AutoTaxDao;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface AutoTaxRepositoryDao extends CrudRepository<AutoTaxDao, Integer> {

}