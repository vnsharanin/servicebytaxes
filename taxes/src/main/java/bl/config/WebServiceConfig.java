package bl.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {
	@Bean
	public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
		MessageDispatcherServlet servlet = new MessageDispatcherServlet();
		servlet.setApplicationContext(applicationContext);
		servlet.setTransformWsdlLocations(true);
		return new ServletRegistrationBean(servlet, "/wsTaxes/*");
	}

	@Bean(name = "taxes")
	public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema taxesSchema) {
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName("TaxesPort");
		wsdl11Definition.setLocationUri("/wsTaxes");
		wsdl11Definition.setTargetNamespace("http://spring.io/getter-taxes-web-service");
		wsdl11Definition.setSchema(taxesSchema);
		return wsdl11Definition;
	}

	@Bean
	public XsdSchema taxesSchema() {
		return new SimpleXsdSchema(new ClassPathResource("Taxes.xsd"));
	}
}
