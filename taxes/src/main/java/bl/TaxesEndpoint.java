package bl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import io.spring.getter_taxes_web_service.GetTaxesRequest;
import io.spring.getter_taxes_web_service.GetTaxesResponse;

@Endpoint
public class TaxesEndpoint {
	private static final String NAMESPACE_URI = "http://spring.io/getter-taxes-web-service";

	private TaxesRepository taxesRepository;

	@Autowired
	public TaxesEndpoint(TaxesRepository taxesRepository) {
		this.taxesRepository = taxesRepository;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getTaxesRequest")
	@ResponsePayload
	public GetTaxesResponse getTaxes(@RequestPayload GetTaxesRequest request) {
		return taxesRepository.findTaxes(request.getINN());
	}
}
