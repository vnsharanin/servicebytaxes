﻿drop database ws_taxes_data;
create database ws_taxes_data;
use ws_taxes_data;

create table ground_taxes (
   id BIGINT NOT NULL AUTO_INCREMENT,

   number NVARCHAR(256) NOT NULL,
   place NVARCHAR(256) NOT NULL,
   sum DOUBLE NULL,
   is_payed TINYINT DEFAULT 0,
   inn NVARCHAR(256) NULL,
   PRIMARY KEY (id)
);

INSERT INTO ground_taxes(number,place,sum,is_payed,inn)
VALUES
	('52:33:24:95','8-Е МАРТА ул, д.В 200 М НА СЕВЕ, Павлово г, Павловский р-н, НИЖЕГОРОДСКАЯ ОБЛ (по ОКАТО 22242501000)',100,0,"1234567890")
,	('52:33:24:95','8-Е МАРТА ул, д.В 200 М НА СЕВЕ, Павлово г, Павловский р-н, НИЖЕГОРОДСКАЯ ОБЛ (по ОКАТО 22242501000)',200,1,"1234567890")
,	('52:33:24:95','8-Е МАРТА ул, д.В 200 М НА СЕВЕ, Павлово г, Павловский р-н, НИЖЕГОРОДСКАЯ ОБЛ (по ОКАТО 22242501000)',300,0,"1234567890");

create table auto_taxes (
   id BIGINT NOT NULL AUTO_INCREMENT,

   object NVARCHAR(256) NOT NULL,
   number NVARCHAR(256) NOT NULL,
   sum DOUBLE NULL,
   is_payed TINYINT DEFAULT 0,
   inn NVARCHAR(256) NULL,
   PRIMARY KEY (id)
);

INSERT INTO auto_taxes(object,number,sum,is_payed,inn)
VALUES
	('2110','T188HM33',100,1,"1234567890")
,	('2110','T188HM33',200,0,"1234567890")
,	('STEPWAY','H849MO33',300,0,"1234567890");