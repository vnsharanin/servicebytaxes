package net.webservicex;

import io.spring.getter_inn_web_service.*;
import io.spring.getter_taxes_web_service.*;
import io.spring.getter_penalties_web_service.*;
import java.util.*;

public class MainClassForCalls {

    public static void main(String[] args) {
		//Summ
		double finalSum = 0;

		//Getting INN
        GetINNRequest INNRequest = new GetINNRequest();

		Passport passport = new Passport();
		passport.setSeria(1714);
		passport.setNumber(950951);

		INNRequest.setPassport(passport);
		INNPortService INNRequestBuilderService = new INNPortService();
        String INN = INNRequestBuilderService.getINNPortSoap11().getINN(INNRequest).getINN();
		System.out.println("INN = "+INN);

		if (INN != null)
		{
			//Getting Taxes
			TaxesPortService TaxesRequestBuilderService = new TaxesPortService();
			GetTaxesRequest taxesRequest = new GetTaxesRequest();
			taxesRequest.setINN(INN);
			List<GroundTax> groundTaxes= TaxesRequestBuilderService.getTaxesPortSoap11().getTaxes(taxesRequest).getGroundTaxes();
			List<AutoTax> autoTaxes = TaxesRequestBuilderService.getTaxesPortSoap11().getTaxes(taxesRequest).getAutoTaxes();

			for(GroundTax groundTax : groundTaxes){
				if (!groundTax.isIsPayed()) {
					finalSum += Double.valueOf(groundTax.getSum());
					System.out.println("finalSum = "+finalSum+" (groundTax)");
				}
			}

			if (autoTaxes.get(0) != null) {
				//Preparing unique umber for GIBDDD
				Set<String> uniqueNumbers = new HashSet<String>();
				for(AutoTax autoTax : autoTaxes){
					uniqueNumbers.add(autoTax.getNumber());
					if (!autoTax.isIsPayed()){
						finalSum += Double.valueOf(autoTax.getSum());
						System.out.println("finalSum = "+finalSum+" (autoTax)");
					}
				}
				//Getting penalties
				GetPenaltiesRequest penaltiesRequest = new GetPenaltiesRequest();
				for(String number : uniqueNumbers){
					penaltiesRequest.getNumber().add(number);
				}
				PenaltiesPortService PenaltiesRequestBuilderService = new PenaltiesPortService();
				List<Penalty> penalties = PenaltiesRequestBuilderService.getPenaltiesPortSoap11().getPenalties(penaltiesRequest).getPenalties();
				for(Penalty penalty : penalties){
					if (!penalty.isIsPayed()){
						finalSum += Double.valueOf(penalty.getSum());
						System.out.println("finalSum = "+finalSum+" (penalty)");
					}
				}
			}
		}
    }
}
